# -*- coding: utf-8 -*-

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django import forms
from .models import Profile


class SignupForm(UserCreationForm):
    screen_name = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True

    class Meta:
        model = User
        fields = ("screen_name", "username", "email", "password1", "password2")

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            _("A user with that email already exists."))

    def save(self, commit=True):
        user = super(SignupForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class ProfileEditForm(forms.ModelForm):

    def clean_name(self):
        name = self.cleaned_data["name"]
        try:
            Profile.objects.get(name=name)
        except User.DoesNotExist:
            return name
        raise forms.ValidationError(
            _("A profile with that name already exists."))

    class Meta:
        model = Profile
        fields = ['name', 'thumbnail', 'homepage', 'self_introduction']
