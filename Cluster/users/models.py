# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from .manager import FollowManager
from model_utils.models import TimeStampedModel
from django.db.models.signals import post_save


class Picture(TimeStampedModel):
    """
    Simple Picture
    """
    user = models.ForeignKey(User)
    picture = models.ImageField(
        upload_to="images/",
        default='/images/thumbnails/anonymous.png')

    def __str__(self):
        return str(self.picture)


class Profile(TimeStampedModel):
    """
    プロフィール
    """
    name = models.CharField(_('accounts name'), max_length=24)
    user = models.OneToOneField(User)
    thumbnail = models.ForeignKey(Picture)
    homepage = models.URLField(_('homepage'), blank=True)
    self_introduction = models.TextField(_('self introduction'), blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        picture = Picture.objects.create(user=self.user)
        picture.save()
        self.name = self.user.username
        self.thumbnail = picture
        super(Profile, self).save(*args, **kwargs)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = Profile.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)


User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])


class Follow(TimeStampedModel):
    """
    フォローモデル
    """
    from_user = models.ForeignKey(User)
    target_user = models.ForeignKey(User, related_name='followed_by')
    is_active = models.BooleanField(default=True)
    objects = FollowManager()

    def __str__(self):
        return '%s is following %s.' % (
            self.from_user.username, self.target_user.username)
