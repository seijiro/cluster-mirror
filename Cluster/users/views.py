# -*- coding: utf-8 -*-
from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import DetailView, ListView, CreateView, TemplateView
from django.views.generic.edit import UpdateView, FormView
from django.contrib.auth.models import User
from .models import Profile, Follow


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'


class FollowingListView(ListView):
    template_name = 'users/views/following.html'
    slug_field = 'name'
    slug_url_kwarg = 'user_name'
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        user = get_object_or_404(User, username=self.kwargs['user_name'])
        self.object = user.profile
        return super(FollowingListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FollowingListView, self).get_context_data(**kwargs)
        context['target_user'] = self.object.user
        return context

    def get_queryset(self):
        return Follow.objects.get_following(self.object.user, count=5)


class FollowerListView(ListView):
    template_name = 'users/views/follower.html'
    slug_url_kwarg = 'user_name'

    def get(self, request, *args, **kwargs):
        self.object = get_object_or_404(User, username=self.kwargs['user_name'])
        return super(FollowerListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FollowerListView, self).get_context_data(**kwargs)
        context['login_user'] = self.request.user
        context['target_user'] = get_object_or_404(User, username=self.kwargs['user_name'])
        return context

    def get_queryset(self):
        return Follow.objects.get_follower(self.object, count=20)


class ProfileView(DetailView):
    model = Profile
    slug_url_kwarg = 'user_name'
    template_name = 'users/views/profile.html'

    def get_object(self, queryset=None):
        return get_object_or_404(User, username=self.kwargs['user_name'])

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        followers = Follow.objects.get_follower(self.object, count=5)
        context['followers'] = followers
        followings = Follow.objects.get_following(self.object)
        context['followings'] = followings
        return context


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    fields = ['name', 'thumbnail', 'homepage', 'self_introduction']
    template_name = 'users/views/edit_profile.html'
    success_url = 'home'

    def get_object(self, queryset=None):
        return Profile.objects.get(user=self.request.user)

    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(self.success_url)

    def form_valid(self, form):
        if form.is_valid():
            form.save()
            return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        context['user'] = self.object.user
        return context


class LaundingPageView(TemplateView):
   template_name = "users/views/launding_page.html"
