# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import (
    ProfileUpdateView,
    ProfileView,
    FollowingListView,
    FollowerListView,
    LaundingPageView,
    DashboardView)


urlpatterns = patterns('',
    url(r'^$', DashboardView.as_view(), name='home'),
    url(r'^profile/(?P<user_name>\w+)/$', ProfileView.as_view(), name='profile'),
    url(r'^following/(?P<user_name>\w+)/$', FollowingListView.as_view(), name='following'),
    url(r'^follower/(?P<user_name>\w+)/$', FollowerListView.as_view(), name='follower'),
    url(r'^settings/profile/$', ProfileUpdateView.as_view(), name='settings'),
    url(r'^launding/$', LaundingPageView.as_view(), name="launding"),
)
